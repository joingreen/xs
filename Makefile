mongo:
	docker-compose up mongo
redis:
	docker-compose up redis
up:
	docker-compose up
rmdb:
	rm -rf docker/db
down:
	docker-compose down
stop:
	docker stop $(docker ps -a)
rm:
	docker rm $(docker ps -a)
sh: 
	docker exec -it sugu_db_1 sh
build:
	sudo docker build -t xs .