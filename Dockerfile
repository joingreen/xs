FROM node:15.5.1-alpine3.10

WORKDIR /app

ENV TZ=Asia/Ho_Chi_Minh

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo $TZ > /etc/timezone

RUN apk update

COPY . /app

RUN yarn install

#RUN chmod +x /usr/bin/entrypoint.sh
#ENTRYPOINT ["entrypoint.sh"]
EXPOSE 8000

CMD ["node", "src/server.js"]