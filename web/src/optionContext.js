import React from 'react';

const opContext = React.createContext({
  num:0,
  mini:true,
  setNum: () => {},
  setMini: () => {},
});


const OptionProvider = (props) => {
const [num, setN]=React.useState(0)
const [mini, setM]=React.useState(true)
const setNum = (num) => {
  setN(num)
};
const setMini = () => {
  setM(!mini)
};
return (
    <opContext.Provider value={[num, mini, setNum, setMini]}>
      {props.children}
    </opContext.Provider>
    )
}

export default OptionProvider
export {opContext}