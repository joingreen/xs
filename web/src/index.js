import React from 'react';
import ReactDOM from 'react-dom';
import './reset.css'
import './index.css';
import App from './App';
import OptionProvider from './optionContext'

ReactDOM.render(
  <OptionProvider>
    <App />
  </OptionProvider>,
  document.getElementById('root')
);


