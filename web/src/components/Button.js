import React from "react";
import { createUseStyles } from 'react-jss';
const useStyles = createUseStyles({
  myClass: {
    margin: {
      top: 16
    },
    backgroundColor: "green",
    padding: 10,
    color: "white",
    border: "none",
    cursor: "pointer"
  }
});
function Button({ onClick, children }) {
    const classes = useStyles();
    return (

        <button className={classes.myClass} onClick={onClick}> {children} </button>
    );
}
export default Button;