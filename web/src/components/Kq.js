import React, { useEffect } from 'react'
import io from 'socket.io-client'
import './kq.scss'
import {opContext} from '../optionContext'
//const socket = io('http://127.0.0.1:8000')

const Kq=({firstKq})=>{
  let [num,mini,setNum,setMini]=React.useContext(opContext)
  let [kq2, kq3]=raw(firstKq)
  let miniKq=kqInMiniTable(kq2)
  console.log(miniKq)
  let kq=firstKq
  if(num===2) kq=kq2
  if(num===3) kq=kq3
  let mienString=kq[0].matinh<25?'Miền Nam':(kq[0].matinh>40?'Miền Bắc':'Miền Trung')
  let isOneKq=kq.length===1
  let isMn=kq[0].matinh<40
  let thuString=''
  switch(kq[0].thu){
    case 0: thuString='Chủ Nhật,';break
    case 1: thuString='Thứ Hai,';break
    case 2: thuString='Thứ Ba,';break
    case 3: thuString='Thứ Tư,';break
    case 4: thuString='Thứ Năm,';break
    case 5: thuString='Thứ Sáu,';break
    case 6: thuString='Thứ Bảy,';break
    default: thuString=''
  }
  return(
    <div className='trang'>
      <div className='tit'>
        <h1>{`Xổ Số ${mienString} `}</h1>
        <h3 className='subcontent'>{`${thuString} ${(kq[0].ngay).split("-").join(" - ")}`}</h3>
      </div>
      {(isMn && !isOneKq)?
      <table>
        <tbody>
        <tr>
          <th>Giải</th>
          {kq.map((v,i)=> <td className='tinh' key={i}>{tenTinh(v.matinh)}</td>)}
        </tr>
        <tr>
          <th>G8</th>
          {kq.map((v,i)=> <td className='giaitam' key={i}>{v.g8}</td>)}
        </tr>
        <tr>
          <th>G7</th>
          {kq.map((v,i)=> <td key={i}>{v.g7[0]}</td>)}
        </tr>
        <tr>
          <th>G6</th>
          {kq.map((v,i)=> <td key={i}><div>{v.g6[0]}</div><div>{v.g6[1]}</div><div>{v.g6[2]}</div></td>)}
        </tr>
        <tr>
          <th>G5</th>
          {kq.map((v,i)=> <td key={i}>{v.g5[0]}</td>)}
        </tr>
        <tr>
          <th>G4</th>
          {kq.map((v,i)=> <td key={i}><div>{v.g4[0]}</div><div>{v.g4[1]}</div><div>{v.g4[2]}</div><div>{v.g4[3]}</div><div>{v.g4[4]}</div><div>{v.g4[5]}</div><div>{v.g4[6]}</div></td>)}
        </tr>
        <tr>
          <th>G3</th>
          {kq.map((v,i)=> <td key={i}><div>{v.g3[0]}</div><div>{v.g3[1]}</div></td>)}
        </tr>
        <tr>
          <th>G2</th>
          {kq.map((v,i)=> <td key={i}>{v.g2[0]}</td>)}
        </tr>
        <tr>
          <th>G1</th>
          {kq.map((v,i)=> <td key={i}>{v.g1}</td>)}
        </tr>
        <tr>
          <th>ĐB</th>
          {kq.map((v,i)=> <td className='giaidb' key={i}>{v.g0}</td>)}
        </tr>
        </tbody>
      </table>
      :((isMn && isOneKq)?
      <table>
        <tbody>
        <tr>
          <th>Giải</th>
          <td className='tinh'>{tenTinh(kq[0].matinh)}</td>
        </tr>
        <tr>
          <th>G8</th>
          <td className='giaitam'>{kq[0].g8}</td>
        </tr>
        <tr>
          <th>G7</th>
          <td>{kq[0].g7[0]}</td>
        </tr>
        <tr>
          <th>G6</th>
          <td><span>{kq[0].g6[0]}</span><span>{kq[0].g6[1]}</span><span>{kq[0].g6[2]}</span></td>
        </tr>
        <tr>
          <th>G5</th>
          <td>{kq[0].g5[0]}</td>
        </tr>
        <tr>
          <th>G4</th>
          <td><div><span>{kq[0].g4[0]}</span><span>{kq[0].g4[1]}</span><span>{kq[0].g4[2]}</span><span>{kq[0].g4[3]}</span></div><div><span>{kq[0].g4[4]}</span><span>{kq[0].g4[5]}</span><span>{kq[0].g4[6]}</span></div></td>
        </tr>
        <tr>
          <th>G3</th>
          <td><span>{kq[0].g3[0]}</span><span>{kq[0].g3[1]}</span></td>
        </tr>
        <tr>
          <th>G2</th>
          <td>{kq[0].g2[0]}</td>
        </tr>
        <tr>
          <th>G1</th>
          <td>{kq[0].g1}</td>
        </tr>
        <tr>
          <th>ĐB</th>
          <td className='giaidb'>{kq[0].g0}</td>
        </tr>
        </tbody>
      </table>
      :
      <table>
        <tbody>
        <tr>
          <th>Giải</th>
          <td className='tinh'>{tenTinh(kq[0].matinh)}</td>
        </tr>
        <tr>
          <th>ĐB</th>
          <td className='giaitam'>{kq[0].g0}</td>
        </tr>
        <tr>
          <th>G1</th>
          <td>{kq[0].g1}</td>
        </tr>
        <tr>
          <th>G2</th>
          <td><span>{kq[0].g2[0]}</span><span>{kq[0].g2[1]}</span></td>
        </tr>
        <tr>
          <th>G3</th>
          <td><div><span>{kq[0].g3[0]}</span><span>{kq[0].g3[1]}</span><span>{kq[0].g3[2]}</span></div><div><span>{kq[0].g3[3]}</span><span>{kq[0].g3[4]}</span><span>{kq[0].g3[5]}</span></div></td>
        </tr>
        <tr>
          <th>G4</th>
          <td><span>{kq[0].g4[0]}</span><span>{kq[0].g4[1]}</span><span>{kq[0].g4[2]}</span><span>{kq[0].g4[3]}</span></td>
        </tr>
        <tr>
          <th>G5</th>
          <td><div><span>{kq[0].g5[0]}</span><span>{kq[0].g5[1]}</span><span>{kq[0].g5[2]}</span></div><div><span>{kq[0].g5[3]}</span><span>{kq[0].g5[4]}</span><span>{kq[0].g5[5]}</span></div></td>
        </tr>
        <tr>
          <th>G6</th>
          <td><span>{kq[0].g6[0]}</span><span>{kq[0].g6[1]}</span><span>{kq[0].g6[2]}</span></td>
        </tr>
        <tr>
          <th>G7</th>
          <td className='giaitam'><span>{kq[0].g7[0]}</span><span>{kq[0].g7[1]}</span><span>{kq[0].g7[2]}</span><span>{kq[0].g7[3]}</span></td>
        </tr>
        </tbody>
      </table>)
      }
      <div className='tool'>
        <div className='option'>
          <button style={{color:num===0?'#03C':'inherit'}} onClick={()=>{setNum(0)}}>Đầy đủ</button>
          <button style={{color:num===2?'#03C':'inherit'}} onClick={()=>{setNum(2)}}>2 số</button>
          <button style={{color:num===3?'#03C':'inherit'}} onClick={()=>{setNum(3)}}>3 số</button>
        </div>
        <div className='mini'>
          <button style={{color:mini?'#03C':'inherit'}} onClick={()=>setMini()}>Bảng Loto</button>
        </div>
      </div>
      <div className='minikq'>
       <MiniTable kq={miniKq}/>
      </div>
    </div>
  )
}
export default Kq

const MiniTable=({kq})=>{
  return(
    <div className='wrapmini'>
      {(kq[0].dau).map((v,i)=>(
        <div className='rmini' key={i}>
          <div className='c1mini'>{i}</div>
          <div className='c2mini'>{kq.map((vv,ii)=><div key={ii} style={{textAlign:'left',width:'100%'}}>{vv.dau[i].join(', ')}</div>)}</div>
        </div>
      ))}
    </div>
  )
}
function kqInMiniTable(kq2){
  let final=[]

  kq2.map((v,i)=>{
    let tmp=JSON.stringify(v).match(/(?<=")\d+(?=")/g)
    let dau=[[],[],[],[],[],[],[],[],[],[]]
    let duoi=[[],[],[],[],[],[],[],[],[],[]]  

    tmp.map(v=>{
      dau[v.slice(0,1).toString()].push(v.slice(1,2))
      duoi[v.slice(1,2).toString()].push(v.slice(0,1))
    })
    final.push({dau, duoi})
  })
  return final
}

function raw(kq){
  let s=JSON.stringify(kq)
  let rawAr=s.match(/(?<=")\d+(?=")/g)
  let srawAr=JSON.stringify(rawAr)
  let raw2=srawAr.match(/\d{2}(?=")/g)
  let raw3=srawAr.match(/\d{2,3}(?=")/g)
  
  let r2=s
  let r3=s

  for (let i in rawAr){
    r2=r2.replace(rawAr[i],raw2[i])
    
    r3=r3.replace(rawAr[i],raw3[i])
  }
  return[JSON.parse(r2),JSON.parse(r3)]
}

const tenTinh=(i)=>{
  switch(i){
      case 1:return "TP. HCM"
      case 2:return "Đồng Tháp"
      case 3:return "Cà Mau"
      case 7:return "Bến Tre"
      case 8:return "Vũng Tàu"
      case 9:return "Bạc Liêu"
      case 10:return "Đồng Nai"
      case 11:return "Cần Thơ"
      case 12:return "Sóc Trăng"
      case 13:return "Tây Ninh"
      case 14:return "An Giang"
      case 15:return "Bình Thuận"
      case 16:return "Vĩnh Long"
      case 17:return "Bình Dương"
      case 18:return "Trà Vinh"
      case 19:return "Long An"
      case 20:return "Hậu Giang"
      case 21:return "Bình Phước"
      case 22:return "Tiền Giang"
      case 23:return "Kiên Giang"
      case 24:return "Đà Lạt"
      
      case 26:return "Thừa T. Huế"
      case 27:return "Phú Yên"
      case 28:return "Quảng Nam"
      case 29:return "Đắk Lắk"
      case 30:return "Đà Nẵng"
      case 31:return "Khánh Hòa"
      case 32:return "Bình Định"
      case 33:return "Quảng Trị"
      case 34:return "Quảng Bình"
      case 35:return "Gia Lai"
      case 36:return "Ninh Thuận"
      case 37:return "Quảng Ngãi"
      case 38:return "Đắk Nông"
      case 39:return "Kon Tum"

      case 46:return "Hà Nội"
      case 47:return "Quảng Ninh"
      case 48:return "Bắc Ninh"
      case 49:return "Hải Phòng"
      case 50:return "Nam Định"
      case 51:return "Thái Bình"
  }
}

const Title=({thu, ngay})=>{
  let [scroll, setScroll] = React.useState(false)
  let isHomnay=Date.now()- (new Date(ngay.split("-").reverse().join("-"))).getTime()>0
  let thuString=''
  switch(thu){
    case 0: thuString='CHỦ NHẬT';break
    case 1: thuString='THỨ HAI';break
    case 2: thuString='THỨ BA';break
    case 3: thuString='THỨ TƯ';break
    case 4: thuString='THỨ NĂM';break
    case 5: thuString='THỨ SÁU';break
    case 6: thuString='THỨ BẢY';break
  }
  useEffect(()=>{
    function handleSroll(){
      let scrollTop = document.body.scrollTop > 40 || document.documentElement.scrollTop > 400
      setScroll(scrollTop)
    }
    window.addEventListener('scroll', handleSroll)
    return () => window.removeEventListener('scroll', handleSroll)
  })
    return(
      <div className='title'>
        <div className='subcontent'>{isHomnay && 'Hôm nay'}</div>
        <div className='content'>
          <div>{`${thuString}, ${ngay.split("-").join(" - ")}`}</div>
          {isHomnay && <div className='today'>Hôm nay</div>}
        </div>
      </div>
    )
}


  

