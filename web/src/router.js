import createBrowserRouter from 'react-concurrent-router/createBrowserRouter'
 
const routes = [
  {
    path: '/',
    component: () => import('./page/Home'),
    prefetch: () => ({ popularProducts: fetch('https://.../api/fetchPopularProducts') }),
    children: [
      { path: 't', component: () => import('./page/Trung') },
      { path: 'n', component: () => import('./page/Nam')},
      { path: 'b', component: () => import('./page/Bac') },
     
    ]
  }
]
const router = createBrowserRouter({ routes })
 
export default router
 
// src/App.js
