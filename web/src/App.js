import './App.scss';
import {Suspense, lazy, useEffect } from 'react'
import { BrowserRouter, Switch, Route, Link, NavLink } from "react-router-dom";


const Home = lazy(() => import('./page/Home'))
const Nam = lazy(() => import('./page/Nam'))
const Trung= lazy(() => import('./page/Trung'))
const Bac = lazy(() => import('./page/Bac'))


function App() {
  
  useEffect(()=>{

  })
  return (
    
    <div className="App">
      <BrowserRouter>
      <header className="header">
        <div className='logo'><Link to="/">Logo.com</Link></div>
        <nav>
            <NavLink to="/mien-nam">Miền Nam</NavLink>
            <NavLink to="/mien-trung">Miền Trung</NavLink>
            <NavLink to="/mien-bac">Miền Bắc</NavLink>
        </nav>
    </header>
      <article>
      <Suspense fallback={<div>Loading...</div>}> 
        <Switch>
          <Route component={Home} path="/" exact />
          <Route component={Nam} path="/mien-nam" />
          <Route component={Trung} path="/mien-trung" />
          <Route component={Bac} path="/mien-bac" />
        </Switch>
      </Suspense>
      </article> 
      </BrowserRouter>
    </div>
  );
}

export default App;
