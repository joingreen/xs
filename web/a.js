
let ngay='03-04-2021'
let m=ngay.match(/\d+/g)
let tmpKq=[
    {
      g2: [ '26847' ],
      g3: [ '60052', '95059' ],
      g4: [
        '94753', '92311',
        '17253', '92784',
        '08971', '19735',
        '88842'
      ],
      g5: [ '0979' ],
      g6: [ '4563', '1968', '9057' ],
      g7: [ '124' ],
      thu: 2,
      ngay: '31-03-2020',
      matinh: 7,
      g0: '315960',
      g1: '89565',
      g8: '89'
    },
    {
      g2: [ '31899' ],
      g3: [ '91232', '20118' ],
      g4: [
        '05871', '59707',
        '12293', '73984',
        '07483', '78308',
        '73836'
      ],
      g5: [ '6081' ],
      g6: [ '2031', '1407', '0135' ],
      g7: [ '440' ],
      thu: 2,
      ngay: '31-03-2020',
      matinh: 8,
      g0: '351163',
      g1: '92536',
      g8: '08'
    }
  ]


function raw(kq){
  let s=JSON.stringify(tmpKq)
  let rawAr=s.match(/(?<=")\d+(?=")/g)
  let srawAr=JSON.stringify(rawAr)
  let raw2=srawAr.match(/\d{2}(?=")/g)
  let raw3=srawAr.match(/\d{2,3}(?=")/g)
  
  let r2=s
  let r3=s

  for (i in rawAr){
    r2=r2.replace(rawAr[i],raw2[i])
    
    r3=r3.replace(rawAr[i],raw3[i])
  }
  return[JSON.parse(r2),JSON.parse(r3)]
}
let kq2=raw()[0]
//console.log(kq2)
function kqInMiniTable(kq2){
  let final=[]

  kq2.map((v,i)=>{
    let tmp=JSON.stringify(v).match(/(?<=")\d+(?=")/g)
    let dau=[[],[],[],[],[],[],[],[],[],[]]
    let duoi=[[],[],[],[],[],[],[],[],[],[]]  

    tmp.map(v=>{
      dau[v.slice(0,1).toString()].push(v.slice(1,2))
      duoi[v.slice(1,2).toString()].push(v.slice(0,1))
    })
    final.push({dau, duoi})
  })
  return final
}
kqInMiniTable(kq2)