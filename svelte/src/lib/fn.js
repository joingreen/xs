export function getThu(num){
    let thuStr
    switch(num){
        case 0: thuStr='Chủ Nhật';break
        case 1: thuStr='Thứ Hai';break
        case 2: thuStr='Thứ Ba';break
        case 3: thuStr='Thứ Tư';break
        case 4: thuStr='Thứ Năm';break
        case 5: thuStr='Thứ Sáu';break
        case 6: thuStr='Thứ Bảy';break
        default: thuStr=''
    }
    return thuStr
}
export const grid=(numOfTinhs, numOfCell)=>{
    if(numOfTinhs.length>1) return numOfTinhs.length
    if(numOfCell==6||numOfCell==7){
        return 3
    }
    return numOfCell
}
export const getTenTinh=(code)=>{
    let tinh=[]
    let mien='Xổ số miền Trung'
    if(code[0]<25) mien='Xổ số miền Nam'
    if(code[0]>40) mien='Xổ số miền Bắc'
    code.map(v=>{
        switch(v){
            case 1:tinh.push("TP. HCM");break
            case 2:tinh.push("Đồng Tháp");break
            case 3:tinh.push("Cà Mau");break
            case 7:tinh.push("Bến Tre");break
            case 8:tinh.push("Vũng Tàu");break
            case 9:tinh.push("Bạc Liêu");break
            case 10:tinh.push("Đồng Nai");break
            case 11:tinh.push("Cần Thơ");break
            case 12:tinh.push("Sóc Trăng");break
            case 13:tinh.push("Tây Ninh");break
            case 14:tinh.push("An Giang");break
            case 15:tinh.push("Bình Thuận");break
            case 16:tinh.push("Vĩnh Long");break
            case 17:tinh.push("Bình Dương");break
            case 18:tinh.push("Trà Vinh");break
            case 19:tinh.push("Long An");break
            case 20:tinh.push("Hậu Giang");break
            case 21:tinh.push("Bình Phước");break
            case 22:tinh.push("Tiền Giang");break
            case 23:tinh.push("Kiên Giang");break
            case 24:tinh.push("Đà Lạt");break
            
            case 26:tinh.push("Thừa T. Huế");break
            case 27:tinh.push("Phú Yên");break
            case 28:tinh.push("Quảng Nam");break
            case 29:tinh.push("Đắk Lắk");break
            case 30:tinh.push("Đà Nẵng");break
            case 31:tinh.push("Khánh Hòa");break
            case 32:tinh.push("Bình Định");break
            case 33:tinh.push("Quảng Trị");break
            case 34:tinh.push("Quảng Bình");break
            case 35:tinh.push("Gia Lai");break
            case 36:tinh.push("Ninh Thuận");break
            case 37:tinh.push("Quảng Ngãi");break
            case 38:tinh.push("Đắk Nông");break
            case 39:tinh.push("Kon Tum");break
    
            case 46:tinh.push("Hà Nội");break
            case 47:tinh.push("Quảng Ninh");break
            case 48:tinh.push("Bắc Ninh");break
            case 49:tinh.push("Hải Phòng");break
            case 50:tinh.push("Nam Định");break
            case 51:tinh.push("Thái Bình");break
        }
    })
    return [mien, tinh]
}
//let kq=[{"thu":2,"ngay":"27-04-2021","matinh":7,"g0":"308101","g1":"26905","g2":"93284","g3":["28250","86264"],"g4":["17641","73114","37211","41186","17548","73482","36011"],"g5":["8911"],"g6":["7274","9067","7310"],"g7":["202"],"g8":"49"},{"thu":2,"ngay":"27-04-2021","matinh":8,"g0":"963798","g1":"18775","g2":"43063","g3":["46256","03002"],"g4":["18854","46163","70598","79493","82066","00778","33439"],"g5":["9203"],"g6":["1638","3153","0019"],"g7":["197"],"g8":"23"},{"thu":2,"ngay":"27-04-2021","matinh":9,"g0":"322012","g1":"49495","g2":"85341","g3":["11478","91281"],"g4":["08622","24378","37551","60997","09849","61082","97771"],"g5":["6112"],"g6":["4300","0207","2688"],"g7":["400"],"g8":"63"}]

export function raw(kq){
    let s=JSON.stringify(kq)
    let s3=JSON.stringify(kq)
    let raw=s.match(/"\d+"/g)

    raw.map(v=>{
        s=s.replace(v,'"'+v.match(/\d{2}"/))
        s3=s3.replace(v,'"'+v.match(/\d{2,3}"/))
    })

    let dau=[[],[],[],[],[],[],[],[],[],[]]
    let duoi=[[],[],[],[],[],[],[],[],[],[]]
    let len=kq.code.length
    for (let v in kq.code){
        let dau1=[[],[],[],[],[],[],[],[],[],[]]
        let duoi1=[[],[],[],[],[],[],[],[],[],[]]
        for (let k in kq.kq){
            
            let kqlen=kq.kq[k].length
            let inlen=kqlen/len
            let start=v*inlen
            let stop=(Number(v)+1)*inlen
            for (let i=start; i<stop; i++){
               
                let pcs=kq.kq[k][i]
                console.log(pcs)
                dau1[Number(pcs[pcs.length-2])].push(pcs[pcs.length-1])
                duoi1[Number(pcs[pcs.length-1])].push(pcs[pcs.length-2])
            } 
        }
        for (let i=0; i<10; i++){
            dau[i].push(dau1[i])
            duoi[i].push(duoi1[i])
        }
    }

    return {s2:JSON.parse(s) , s3:JSON.parse(s3), mini:{dau, duoi}}
}
/*

setInterval(()=>{
    for (i = 0; i < 24; i++) {
        $('.random:eq('+ i +')').html(Math.floor((Math.random()*10)))
    }
},100)
*/
