import { writable } from 'svelte/store';

function createKqtt() {
	const { subscribe, set} = writable({});
	return {
		subscribe,
		set
	}
}
function createOption() {
	const { subscribe, update } = writable({num:0, loto:false});

	return {
		subscribe,
		set0: () => update(self => {
			self.num = 0
			return self
		}),
		set3: () => update(self => {
			self.num = 3
			return self
		}),
		set2: () => update(self => {
			self.num = 2
			return self
		}),
		setLoto: () => update(self => {
			self.loto = !self.loto
			return self
		})

	};
}

export let opt = createOption();
export let kqtt = createKqtt()
