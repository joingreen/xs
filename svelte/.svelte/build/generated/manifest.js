const c = [
	() => import("../../../src/routes/$layout.svelte"),
	() => import("../components/error.svelte"),
	() => import("../../../src/routes/index.svelte"),
	() => import("../../../src/routes/settings/$layout.svelte"),
	() => import("../../../src/routes/settings/index.svelte"),
	() => import("../../../src/routes/settings/profile.svelte"),
	() => import("../../../src/routes/xsmb/index.svelte"),
	() => import("../../../src/routes/xsmn/index.svelte"),
	() => import("../../../src/routes/xsmt/index.svelte")
];

const d = decodeURIComponent;

export const routes = [
	// src/routes/index.json.js
	[/^\/index\.json$/],

	// src/routes/index.svelte
	[/^\/$/, [c[0], c[2]], [c[1]]],

	// src/routes/settings/index.svelte
	[/^\/settings\/?$/, [c[0], c[3], c[4]], [c[1]]],

	// src/routes/settings/profile.svelte
	[/^\/settings\/profile\/?$/, [c[0], c[3], c[5]], [c[1]]],

	// src/routes/xsmb/index.svelte
	[/^\/xsmb\/?$/, [c[0], c[6]], [c[1]]],

	// src/routes/xsmn/index.svelte
	[/^\/xsmn\/?$/, [c[0], c[7]], [c[1]]],

	// src/routes/xsmt/index.svelte
	[/^\/xsmt\/?$/, [c[0], c[8]], [c[1]]]
];

export const fallback = [c[0](), c[1]()];