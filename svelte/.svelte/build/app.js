import { ssr } from '@sveltejs/kit/ssr';
import root from './generated/root.svelte';
import { set_paths } from './runtime/paths.js';
import { set_prerendering } from './runtime/env.js';
import * as user_hooks from "./hooks.js";

const template = ({ head, body }) => "<!DOCTYPE html>\n<html lang=\"en\">\n\t<head>\n\t\t<meta charset=\"utf-8\" />\n\t\t<link rel=\"icon\" href=\"/favicon.ico\" />\n\t\t<link rel=\"stylesheet\" href=\"app.css\">\n\t\t<link rel=\"stylesheet\" href=\"table.css\">\n\t\t<link rel=\"stylesheet\" href=\"full.css\" />\n<!-- \n<link rel=\"stylesheet\" href=\"css/buttons.css\" />\n\n<link rel=\"stylesheet\" href=\"css/cards.css\" />\n\n<link rel=\"stylesheet\" href=\"css/color_palette.css\" />\n\n<link rel=\"stylesheet\" href=\"css/forms.css\" />\n\n<link rel=\"stylesheet\" href=\"css/layout.css\" />\n\n<link rel=\"stylesheet\" href=\"css/modals.css\" />\n-->\n\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n\t\t" + head + "\n\t</head>\n\t<body>\n\t\t<div id=\"svelte\">" + body + "</div>\n\t</body>\n</html>\n";

let options = null;

// allow paths to be overridden in svelte-kit start
// and in prerendering
export function init(settings) {
	set_paths(settings.paths);
	set_prerendering(settings.prerendering || false);

	options = {
		amp: false,
		dev: false,
		entry: {
			file: "/./_app/start-8a2e57ec.js",
			css: ["/./_app/assets/start-a8cd1609.css"],
			js: ["/./_app/start-8a2e57ec.js","/./_app/chunks/index-8c696c95.js","/./_app/chunks/index-0f3e6a6d.js"]
		},
		fetched: undefined,
		get_component_path: id => "/./_app/" + entry_lookup[id],
		get_stack: error => String(error), // for security
		handle_error: error => {
			console.error(error.stack);
			error.stack = options.get_stack(error);
		},
		hooks: get_hooks(user_hooks),
		hydrate: true,
		initiator: undefined,
		load_component,
		manifest,
		paths: settings.paths,
		read: settings.read,
		root,
		router: true,
		ssr: true,
		target: "#svelte",
		template
	};
}

const d = decodeURIComponent;
const empty = () => ({});

const manifest = {
	assets: [{"file":"actions.css","size":2486,"type":"text/css"},{"file":"app.css","size":828,"type":"text/css"},{"file":"buttons.css","size":3020,"type":"text/css"},{"file":"cards.css","size":970,"type":"text/css"},{"file":"color_palette.css","size":7874,"type":"text/css"},{"file":"dark_mode.css","size":1731,"type":"text/css"},{"file":"favicon.ico","size":1150,"type":"image/vnd.microsoft.icon"},{"file":"forms.css","size":11211,"type":"text/css"},{"file":"full.css","size":27787,"type":"text/css"},{"file":"layout.css","size":473,"type":"text/css"},{"file":"modals.css","size":2142,"type":"text/css"},{"file":"newfull.css","size":317,"type":"text/css"},{"file":"robots.txt","size":67,"type":"text/plain"},{"file":"segmented-controls.css","size":1877,"type":"text/css"},{"file":"shadows.css","size":670,"type":"text/css"},{"file":"table.css","size":1255,"type":"text/css"},{"file":"tabs.css","size":2988,"type":"text/css"}],
	layout: "src/routes/$layout.svelte",
	error: ".svelte/build/components/error.svelte",
	routes: [
		{
						type: 'endpoint',
						pattern: /^\/index\.json$/,
						params: empty,
						load: () => import("../../src/routes/index.json.js")
					},
		{
						type: 'page',
						pattern: /^\/$/,
						params: empty,
						a: ["src/routes/$layout.svelte", "src/routes/index.svelte"],
						b: [".svelte/build/components/error.svelte"]
					},
		{
						type: 'page',
						pattern: /^\/settings\/?$/,
						params: empty,
						a: ["src/routes/$layout.svelte", "src/routes/settings/$layout.svelte", "src/routes/settings/index.svelte"],
						b: [".svelte/build/components/error.svelte"]
					},
		{
						type: 'page',
						pattern: /^\/settings\/profile\/?$/,
						params: empty,
						a: ["src/routes/$layout.svelte", "src/routes/settings/$layout.svelte", "src/routes/settings/profile.svelte"],
						b: [".svelte/build/components/error.svelte"]
					},
		{
						type: 'page',
						pattern: /^\/xsmb\/?$/,
						params: empty,
						a: ["src/routes/$layout.svelte", "src/routes/xsmb/index.svelte"],
						b: [".svelte/build/components/error.svelte"]
					},
		{
						type: 'page',
						pattern: /^\/xsmn\/?$/,
						params: empty,
						a: ["src/routes/$layout.svelte", "src/routes/xsmn/index.svelte"],
						b: [".svelte/build/components/error.svelte"]
					},
		{
						type: 'page',
						pattern: /^\/xsmt\/?$/,
						params: empty,
						a: ["src/routes/$layout.svelte", "src/routes/xsmt/index.svelte"],
						b: [".svelte/build/components/error.svelte"]
					}
	]
};

// this looks redundant, but the indirection allows us to access
// named imports without triggering Rollup's missing import detection
const get_hooks = hooks => ({
	getContext: hooks.getContext || (() => ({})),
	getSession: hooks.getSession || (() => ({})),
	handle: hooks.handle || (({ request, render }) => render(request))
});

const module_lookup = {
	"src/routes/$layout.svelte": () => import("../../src/routes/$layout.svelte"),".svelte/build/components/error.svelte": () => import("./components/error.svelte"),"src/routes/index.svelte": () => import("../../src/routes/index.svelte"),"src/routes/settings/$layout.svelte": () => import("../../src/routes/settings/$layout.svelte"),"src/routes/settings/index.svelte": () => import("../../src/routes/settings/index.svelte"),"src/routes/settings/profile.svelte": () => import("../../src/routes/settings/profile.svelte"),"src/routes/xsmb/index.svelte": () => import("../../src/routes/xsmb/index.svelte"),"src/routes/xsmn/index.svelte": () => import("../../src/routes/xsmn/index.svelte"),"src/routes/xsmt/index.svelte": () => import("../../src/routes/xsmt/index.svelte")
};

const metadata_lookup = {"src/routes/$layout.svelte":{"entry":"/./_app/pages/$layout.svelte-5019d0d2.js","css":["/./_app/assets/pages/$layout.svelte-21ab21d0.css"],"js":["/./_app/pages/$layout.svelte-5019d0d2.js","/./_app/chunks/index-8c696c95.js","/./_app/chunks/store-6a8d5d2d.js","/./_app/chunks/index-0f3e6a6d.js"],"styles":null},".svelte/build/components/error.svelte":{"entry":"/./_app/error.svelte-a1d0d680.js","css":[],"js":["/./_app/error.svelte-a1d0d680.js","/./_app/chunks/index-8c696c95.js"],"styles":null},"src/routes/index.svelte":{"entry":"/./_app/pages/index.svelte-5c7145b0.js","css":["/./_app/assets/pages/index.svelte-6c285c91.css"],"js":["/./_app/pages/index.svelte-5c7145b0.js","/./_app/chunks/index-8c696c95.js","/./_app/chunks/store-6a8d5d2d.js","/./_app/chunks/index-0f3e6a6d.js"],"styles":null},"src/routes/settings/$layout.svelte":{"entry":"/./_app/pages/settings/$layout.svelte-8efd436e.js","css":[],"js":["/./_app/pages/settings/$layout.svelte-8efd436e.js","/./_app/chunks/index-8c696c95.js"],"styles":null},"src/routes/settings/index.svelte":{"entry":"/./_app/pages/settings/index.svelte-194f7bfb.js","css":[],"js":["/./_app/pages/settings/index.svelte-194f7bfb.js","/./_app/chunks/index-8c696c95.js"],"styles":null},"src/routes/settings/profile.svelte":{"entry":"/./_app/pages/settings/profile.svelte-90adf5f9.js","css":[],"js":["/./_app/pages/settings/profile.svelte-90adf5f9.js","/./_app/chunks/index-8c696c95.js"],"styles":null},"src/routes/xsmb/index.svelte":{"entry":"/./_app/pages/xsmb/index.svelte-175f1358.js","css":[],"js":["/./_app/pages/xsmb/index.svelte-175f1358.js","/./_app/chunks/index-8c696c95.js"],"styles":null},"src/routes/xsmn/index.svelte":{"entry":"/./_app/pages/xsmn/index.svelte-6e1ef7d9.js","css":[],"js":["/./_app/pages/xsmn/index.svelte-6e1ef7d9.js","/./_app/chunks/index-8c696c95.js"],"styles":null},"src/routes/xsmt/index.svelte":{"entry":"/./_app/pages/xsmt/index.svelte-df88994c.js","css":[],"js":["/./_app/pages/xsmt/index.svelte-df88994c.js","/./_app/chunks/index-8c696c95.js"],"styles":null}};

async function load_component(file) {
	return {
		module: await module_lookup[file](),
		...metadata_lookup[file]
	};
}

init({ paths: {"base":"","assets":"/."} });

export function render(request, {
	prerender
} = {}) {
	const host = request.headers["host"];
	return ssr({ ...request, host }, options, { prerender });
}