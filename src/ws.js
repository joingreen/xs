const request = require('request-promise')
const cheerio = require('cheerio')
const cron = require('node-cron')

const WebSocket = require('ws')
const ws = new WebSocket(`ws://127.0.0.1:${process.env.PORT||8000}`)

const dbKq = require('./models/Kq.js')
const moment = require('moment')
const redisClient = require('./models/redis')

const mns=[
    [22,23,24],
    [1,2,3],
    [7,8,9],
    [10,11,12],
    [13,14,15],
    [16,17,18],
    [1,19,20,21]
]
const mts=[
    [39,31],
    [26,27],
    [29,28],
    [30,31],
    [32,33,34],
    [35,36],
    [30,37,38]
]
const mbs=[[51],[46],[47],[48],[46],[49],[50]]
const miens = ['mien-nam', 'mien-trung', 'mien-bac']


async function savedata(kqs){
    try{
        dbKq.insertMany(kqs)
        console.log('data has been saved.')
    }catch(e){
        console.log(e.message)
        miss.push(date)
    } 
}

class TT{
    constructor(date){
        this.now=Date.now()
        this.date=date
        this.y=this.date.getFullYear()
        this.m = this.date.getMonth()
        this.d = this.date.getDate()
        this.day=this.date.getDay()
        this.kqEmit=''
    }
    
    async crawl(mien){
        try{
            console.log(`${mien}: thu ${this.day+1}, ${this.d}-${this.m+1}-${this.y}`)
            let dom =await this.getDOM(miens[mien])
            let kq= mien==2?this.xulymb(dom):this.xulymn(dom, mien)
            console.log(kq)
            kq=JSON.stringify(kq)
            console.log('-------------')
            ws.send(kq)
            let saveRedis=await redisClient.setAsync("kqttInit", kq)
            console.log('save kqttInit in Redis'+saveRedis)
           /* 
            if(this.kqEmit==kq){
                if(!/\*|\+/.test(kq)){
                    savedata(JSON.parse(kq))
                    switch(mien){
                        case 0:cronmn.stop();break;
                        case 1:cronmt.stop();break;
                        case 2:cronmb.stop();break;
                    }   
                }
            }else{
                this.kqEmit=kq
                ws.send(kq)
                await redisClient.setAsync("kqttInRedis", kq)
            }
             */
        }catch(e){
            console.log(e)
        }   
    }
    //return String [*,+]
    rp(preS){
        let s=preS.replace(/loading\.gif/g,'*')
        s=s.replace(/<span\sclass="\srunLoto\sloto_\d">\d<\/span>/g,'+')
        s=s.replace(/[^\+|\*|\d+]/g,'')
        return s
    }

    xulymn($,mien){
        let mienmap=mien==0?mns[this.day]:mts[this.day]
        let result={
            day : this.day,
            date : moment(this.date).format('DD-MM-YYYY'),
            code : mienmap,
            kq:{
                ĐB:[],
                G1:[],
                G2:[],
                G3:[],
                G4:[],
                G5:[],
                G6:[],
                G7:[],
                G8:[]
            }
        }
        for(let i in mienmap){
            result.kq.ĐB.push(this.rp($('#giaidb_'+mienmap[i]).html()))
            result.kq.G1.push(this.rp($('#giai1_'+mienmap[i]).html()))
            result.kq.G2.push(this.rp($('#giai2_'+mienmap[i]).html()))

            result.kq.G3.push(this.rp($('#giai3_1_'+mienmap[i]).html()))
            result.kq.G3.push(this.rp($('#giai3_2_'+mienmap[i]).html()))

            result.kq.G4.push(this.rp($('#giai4_1_'+mienmap[i]).html()))
            result.kq.G4.push(this.rp($('#giai4_2_'+mienmap[i]).html()))
            result.kq.G4.push(this.rp($('#giai4_3_'+mienmap[i]).html()))
            result.kq.G4.push(this.rp($('#giai4_4_'+mienmap[i]).html()))
            result.kq.G4.push(this.rp($('#giai4_5_'+mienmap[i]).html()))
            result.kq.G4.push(this.rp($('#giai4_6_'+mienmap[i]).html()))
            result.kq.G4.push(this.rp($('#giai4_7_'+mienmap[i]).html()))
                
            result.kq.G5.push(this.rp($('#giai5_'+mienmap[i]).html()))
            result.kq.G6.push(this.rp($('#giai6_1_'+mienmap[i]).html()))
            result.kq.G6.push(this.rp($('#giai6_2_'+mienmap[i]).html()))
            result.kq.G6.push(this.rp($('#giai6_3_'+mienmap[i]).html()))

            result.kq.G7.push(this.rp($('#giai7_'+mienmap[i]).html()))
            result.kq.G8.push(this.rp($('#giai8_'+mienmap[i]).html()))
        }
        return result
    }

    xulymb($){
        let day=this.day
        let result={
            day : this.day,
            date : moment(this.date).format('DD-MM-YYYY'),
            code : mbs[this.day][0],
            kq:{
                ĐB:[],
                G1:[],
                G2:[],
                G3:[],
                G4:[],
                G5:[],
                G6:[],
                G7:[]
            }
        }

        result.kq.ĐB.push(this.rp($('#giaidb_'+mbs[day][0]).html()))
        result.kq.G1.push(this.rp($('#giai1_'+mbs[day][0]).html()))

        result.kq.G2.push(this.rp($('#giai2_1_'+mbs[day][0]).html()))
        result.kq.G2.push(this.rp($('#giai2_2_'+mbs[day][0]).html()))

        result.kq.G3.push(this.rp($('#giai3_1_'+mbs[day][0]).html()))
        result.kq.G3.push(this.rp($('#giai3_2_'+mbs[day][0]).html()))
        result.kq.G3.push(this.rp($('#giai3_3_'+mbs[day][0]).html()))
        result.kq.G3.push(this.rp($('#giai3_4_'+mbs[day][0]).html()))
        result.kq.G3.push(this.rp($('#giai3_5_'+mbs[day][0]).html()))
        result.kq.G3.push(this.rp($('#giai3_6_'+mbs[day][0]).html()))

        result.kq.G4.push(this.rp($('#giai4_1_'+mbs[day][0]).html()))
        result.kq.G4.push(this.rp($('#giai4_2_'+mbs[day][0]).html()))
        result.kq.G4.push(this.rp($('#giai4_3_'+mbs[day][0]).html()))
        result.kq.G4.push(this.rp($('#giai4_4_'+mbs[day][0]).html()))
            
        result.kq.G5.push(this.rp($('#giai5_1_'+mbs[day][0]).html()))
        result.kq.G5.push(this.rp($('#giai5_2_'+mbs[day][0]).html()))
        result.kq.G5.push(this.rp($('#giai5_3_'+mbs[day][0]).html()))
        result.kq.G5.push(this.rp($('#giai5_4_'+mbs[day][0]).html()))
        result.kq.G5.push(this.rp($('#giai5_5_'+mbs[day][0]).html()))
        result.kq.G5.push(this.rp($('#giai5_6_'+mbs[day][0]).html()))

        result.kq.G6.push(this.rp($('#giai6_1_'+mbs[day][0]).html()))
        result.kq.G6.push(this.rp($('#giai6_2_'+mbs[day][0]).html()))
        result.kq.G6.push(this.rp($('#giai6_3_'+mbs[day][0]).html()))

        result.kq.G7.push(this.rp($('#giai7_1_'+mbs[day][0]).html()))
        result.kq.G7.push(this.rp($('#giai7_2_'+mbs[day][0]).html()))
        result.kq.G7.push(this.rp($('#giai7_3_'+mbs[day][0]).html()))
        result.kq.G7.push(this.rp($('#giai7_4_'+mbs[day][0]).html()))

        return result
    }

    getDOM = (mien) => {
        let uri=`https://www.minhngoc.net/xo-so-truc-tiep/${mien}.html`
        const options = {
            uri,
            headers: {
                'User-Agent': 'Request-Promise'
            },
            transform: (body) => {
                return cheerio.load(body)
            }
        }
        return request(options)
    }
}
let dateNOT=new Date()
let now=new Date(dateNOT.getFullYear(),dateNOT.getMonth(),dateNOT.getDate())
let Kq = new TT(now)

cron.schedule('0 0 0 * * *',()=>{
    let dateNOT=new Date()
    now=new Date(dateNOT.getFullYear(),dateNOT.getMonth(),dateNOT.getDate())

    cronmn.start()
    cronmt.start()
    cronmb.start()
})
var cronmn=cron.schedule('*/6 10-40 16 * * *',()=>{
    Kq.crawl(0)
})
var cronmt=cron.schedule('*/6 10-40 17 * * *',()=>{
    Kq.crawl(1)
})
var cronmb=cron.schedule('*/6 * * * * *',()=>{
    Kq.crawl(2)
})

ws.on('open', function open() {
    console.log('ws connected !')
    ws.send(Date.now());
});
  
ws.on('close', function close() {
    console.log('disconnected');
});
/*
ws.on('message', function(data) {
    console.log(`ws nhan ${data}`);

    setInterval(function timeout() {
        ws.send(Date.now());
    }, 1000);
});
*/
