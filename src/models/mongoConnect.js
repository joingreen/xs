const mongoose =require('mongoose')

const mongoUrl = process.env.DATABASE_URL || `mongodb://localhost:27017/xs-local1`


const connectWithRetry = function () {
  return mongoose.connect(mongoUrl, {
      useNewUrlParser: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
      useCreateIndex:true  
    }, (err) => {
    if (err) {
      console.error('Failed to connect to mongo on startup - retrying in 5 sec', err)
      setTimeout(connectWithRetry, 5000)
    }else{
      console.log(`Db =>>> ${mongoUrl}`)
    }
  })
}
connectWithRetry()