const Kq = require('../Kq.js')
require('../mongoConnect.js')
const request = require('request-promise')
const cheerio = require('cheerio')
const moment = require('moment')
const mns=[
  [22,23,24],
  [1,2,3],
  [7,8,9],
  [10,11,12],
  [13,14,15],
  [16,17,18],
  [1,19,20,21]
]
const mts=[
  [39,31],
  [26,27],
  [29,28],
  [30,31],
  [32,33,34],
  [35,36],
  [30,37,38]
]
const mbs=[[51],[46],[47],[48],[46],[49],[50]]
const miens = ['mien-nam', 'mien-trung', 'mien-bac'];


class R{
  constructor(date){
    this.date=date
    this.now=Date.now()
    this.y=date.getFullYear()
    this.m = date.getMonth()
    this.d = date.getDate()
    this.day=date.getDay()
    this.linkdate=`${this.d}-${this.m+1}-${this.y}.html`
  }

  async crawl(){
    console.log(`thu ${this.day+1}, ${this.linkdate}`)
    let mnDOM = getDOM(miens[0],this.linkdate)
    let mtDOM = getDOM(miens[1],this.linkdate)
    let mbDOM = getDOM(miens[2],this.linkdate)

    let [mn,mt,mb]= await Promise.all([mnDOM,mtDOM,mbDOM])
    await xulydata([xulymn(mn),xulymn(mt),xulymb(mb),this.date,this.day])
    return this.date.getTime()
  }

  next(){
    if(this.date.setTime(this.date.getTime()+24*60*60*1000)<this.now) {
      let date=this.date
      this.y=date.getFullYear()
      this.m = date.getMonth()
      this.d = date.getDate()
      this.day=date.getDay()
      this.linkdate=`${this.d}-${this.m+1}-${this.y}.html`
    }else{
      console.log('done')
    }
  }
}

async function xulydata([a,b,c,date,day]){
  date=moment(date).format('DD-MM-YYYY')
  console.log(date)
  console.log()
  try{
    let mnn={
      ĐB : [],
      G1 : [],
      G2 : [],
      G3 : [],
      G4 :[],
      G5 : [],
      G6 : [],
      G7 : [],
      G8 : []
    }
    let mtt={
      ĐB : [],
      G1 : [],
      G2 : [],
      G3 : [],
      G4 :[],
      G5 : [],
      G6 : [],
      G7 : [],
      G8 : []
    }
    for(let i in mns[day]){
      mnn.ĐB.push(...a[i].ĐB)
      mnn.G1.push(...a[i].G1)
      mnn.G2.push(...a[i].G2)
      mnn.G3.push(...a[i].G3)
      mnn.G4.push(...a[i].G4)
      mnn.G5.push(...a[i].G5)
      mnn.G6.push(...a[i].G6)
      mnn.G7.push(...a[i].G7)
      mnn.G8.push(...a[i].G8)
    }
    for(let i in mts[day]){
      mtt.ĐB.push(...b[i].ĐB)
      mtt.G1.push(...b[i].G1)
      mtt.G2.push(...b[i].G2)
      mtt.G3.push(...b[i].G3)
      mtt.G4.push(...b[i].G4)
      mtt.G5.push(...b[i].G5)
      mtt.G6.push(...b[i].G6)
      mtt.G7.push(...b[i].G7)
      mtt.G8.push(...b[i].G8)
    }
    console.log({day:day,date: date,code:mns[day],kq:mnn})
    await Kq.create({day:day,date: date,code:mns[day],kq:mnn})
    await Kq.create({day:day,date: date,code:mts[day],kq:mtt})
    await Kq.create({day:day,date: date,code:mbs[day],kq:c[0]})
  }catch(e){
    console.log(e.message)
    miss.push(date)
  }  
}

const getDOM = (mien,linkdate) => {
    let uri=`https://www.minhngoc.net/ket-qua-xo-so/${mien}/${linkdate}`
    const options = {
      uri,
      headers: {
        'User-Agent': 'Request-Promise'
      },
      transform: (body) => {
        return cheerio.load(body)
      }
    }
    return request(options)
}

function xulymn($){
  let a8=$('.content tbody td .giai8').text()
  let a7=$('.content tbody td .giai7').text()
  let a6=$('.content tbody td .giai6').text()
  let a5=$('.content tbody td .giai5').text()
  let a4=$('.content tbody td .giai4').text()
  let a3=$('.content tbody td .giai3').text()
  let a2=$('.content tbody td .giai2').text()
  let a1=$('.content tbody td .giai1').text()
  let a0=$('.content tbody td .giaidb').text()

  let mn1={
    ĐB : [a0.substr(19,6)],
    G1 : [a1.substr(15,5)],
    G2 : [a2.substr(14,5)],
    G3 : [a3.substr(13,5),a3.substr(18,5)],
    G4 :[a4.substr(13,5),a4.substr(18,5),a4.substr(23,5),a4.substr(28,5),a4.substr(33,5),a4.substr(38,5),a4.substr(43,5)],
    G5 : [a5.substr(14,4)],
    G6 : [a6.substr(14,4),a6.substr(18,4),a6.substr(22,4)],
    G7 : [a7.substr(14,3)],
    G8 : [a8.substr(14,2)]
  }
  let mn2={
    ĐB : [a0.substr(30,6)],
    G1 : [a1.substr(25,5)],
    G2 : [a2.substr(24,5)],
    G3 : [a3.substr(28,5),a3.substr(33,5)],
    G4 :[a4.substr(53,5),a4.substr(58,5),a4.substr(63,5),a4.substr(68,5),a4.substr(73,5),a4.substr(78,5),a4.substr(83,5)],
    G5 : [a5.substr(23,4)],
    G6 : [a6.substr(31,4),a6.substr(35,4),a6.substr(39,4)],
    G7 : [a7.substr(22,3)],
    G8 : [a8.substr(21,2)]
  }
  let mn3={
    ĐB : [a0.substr(41,6)],
    G1 : [a1.substr(35,5)],
    G2 : [a2.substr(34,5)],
    G3 : [a3.substr(43,5),a3.substr(48,5)],
    G4 :[a4.substr(93,5),a4.substr(98,5),a4.substr(103,5),a4.substr(108,5),a4.substr(113,5),a4.substr(118,5),a4.substr(123,5)],
    G5 : [a5.substr(32,4)],
    G6 : [a6.substr(48,4),a6.substr(52,4),a6.substr(56,4)],
    G7 : [a7.substr(30,3)],
    G8 : [a8.substr(28,2)]
  }
  let mn4={
    ĐB : [a0.substr(52,6)],
    G1 : [a1.substr(45,5)],
    G2 : [a2.substr(44,5)],
    G3 : [a3.substr(58,5),a3.substr(63,5)],
    G4 :[a4.substr(133,5),a4.substr(138,5),a4.substr(143,5),a4.substr(148,5),a4.substr(153,5),a4.substr(158,5),a4.substr(163,5)],
    G5 : [a5.substr(41,4)],
    G6 : [a6.substr(65,4),a6.substr(69,4),a6.substr(73,4)],
    G7 : [a7.substr(38,3)],
    G8 : [a8.substr(35,2)]
  }

  return [mn1,mn2,mn3,mn4]
}

function xulymb($){
  let c7=$('.content tbody td .giai7').eq(0).text()
  let c6=$('.content tbody td .giai6').eq(0).text()
  let c5=$('.content tbody td .giai5').eq(0).text()
  let c4=$('.content tbody td .giai4').eq(0).text()
  let c3=$('.content tbody td .giai3').eq(0).text()
  let c2=$('.content tbody td .giai2').eq(0).text()
  let c1=$('.content tbody td .giai1').eq(0).text()
  let c0=$('.content tbody td .giaidb').eq(0).text()

  let mb={
    ĐB : [c0.substr(5,5)],
    G1 : [c1.substr(5,5)],
    G2 : [c2.substr(5,5),c2.substr(10,5)],
    G3 : [c3.substr(5,5),c3.substr(10,5),c3.substr(15,5),c3.substr(20,5),c3.substr(25,5),c3.substr(30,5)],
    G4 :[c4.substr(5,4),c4.substr(9,4),c4.substr(13,4),c4.substr(17,4)],
    G5 : [c5.substr(5,4),c5.substr(9,4),c5.substr(13,4),c5.substr(17,4),c5.substr(21,4),c5.substr(25,4)],
    G6 : [c6.substr(5,3),c6.substr(8,3),c6.substr(11,3)],
    G7 : [c7.substr(5,2),c7.substr(7,2),c7.substr(9,2),c7.substr(11,2)],
  }
  return [mb]
}


(async()=>{
  let newd= new Date()
  let now= newd.getTime()
  let date=new Date(newd.getFullYear(),newd.getMonth()-1,newd.getDate())

  let c=new R(date)
  console.log(c)
  while(date.getTime()<now){
    
    await c.crawl()
    c.next()
  }
})()


