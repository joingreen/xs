const mongoose = require('mongoose')
require( './mongoConnect.js')

const Schema = mongoose.Schema

const KqSchema = new Schema({
    day : Number,
    date : String,
    code : [Number],
    kq:{
        ĐB:[String],
        G1:[String],
        G2:[String],
        G3:[String],
        G4:[String],
        G5:[String],
        G6:[String],
        G7:[String],
        G8:[String] 
    }
})
KqSchema.index({ code: 1, date: 1}, { unique: true })

module.exports = mongoose.model('Kq', KqSchema)