let router = require('express').Router()
const createError = require('http-errors')


router.use('/kqtt', require('./kqtt'))
router.use('/kq', require('./kqtt'))

router.use(function(req, res, next) {
  next(createError(404))
});

router.use(function(err, req, res, next) {
    const _status = err.status || 500;
    res.status(_status);
    res.json({
      message: err.message,
      status : _status
    })
})
module.exports = router