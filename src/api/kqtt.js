const router=require('express').Router()
const redisClient = require('../models/redis')

router.route('/').get(async (req, res) => {
    const kqttInRedis = await redisClient.getAsync("kqttInit");
    return res.json(kqttInRedis)
})

module.exports = router
