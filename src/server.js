require('dotenv').config()
require('./models/mongoConnect')


const app = require('./app')
const http = require('http')
const WebSocket = require('ws')

const port = process.env.PORT||8000
const server = http.createServer(app)
const wss = new WebSocket.Server({ server })

wss.on('connection', function connection(ws) {
  ws.on('message', function (data) {
    wss.clients.forEach(function (client) {
      if (client !== ws && client.readyState === WebSocket.OPEN) {
        client.send(data)
      }
    })
  })
})
/*
const ws = new WebSocket(`ws://127.0.0.1:${process.env.PORT||8000}`)

ws.on('open', function open() {
    console.log('connectedddd');
    ws.send(Date.now());
});
  
ws.on('close', function close() {
    console.log('disconnected');
});

ws.on('message', function(data) {
    console.log(`Roundtrip time: ${Date.now() - data} ms`);

    setTimeout(function timeout() {
        ws.send(Date.now());
    }, 1000);
});
*/
require('./ws')



server.listen(port, () => {
    console.log(`PORT=>>> ${port}`)
})

process.on('uncaughtException', err => {
    console.log('UNCAUGHT EXCEPTION!!! shutting down...')
    console.log(err.name, err.message)
    process.exit(1);
})
process.on('unhandledRejection', err => {
    console.log('UNHANDLED REJECTION!!!  shutting down ...')
    console.log(err.name, err.message)
    httpServer.close(() => {
        process.exit(1)
    })
})